pam-p11 (0.3.1-2) UNRELEASED; urgency=low

  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Debian Janitor <janitor@jelmer.uk>  Fri, 24 Jul 2020 22:04:03 -0000

pam-p11 (0.3.1-1) unstable; urgency=medium

  * New upstream release (Closes: 925802, 939664)

 -- Eric Dorland <eric@debian.org>  Mon, 28 Oct 2019 01:36:24 -0400

pam-p11 (0.3.0-1) unstable; urgency=medium

  * New upstream release
  * Drop upstreamed patches
  * Switch to debhelper compat level 12
  * Standards-Version to 4.4.0.0

 -- Eric Dorland <eric@debian.org>  Sun, 07 Jul 2019 17:10:13 -0400

pam-p11 (0.2.0-2) unstable; urgency=medium

  * Cherrypick unsigned comparison fix (Closes: 906679)
  * Standards-Version to 4.2.1.4
  * Add Rules-Requires-Root: no

 -- Eric Dorland <eric@debian.org>  Sat, 03 Nov 2018 00:04:44 -0400

pam-p11 (0.2.0-1) unstable; urgency=medium

  * New upstream release
  * Drop upstreamed patches
  * Add 0001-Allow-compilation-with-newer-openssl-version.patch

 -- Eric Dorland <eric@debian.org>  Wed, 18 Jul 2018 23:35:44 -0400

pam-p11 (0.1.6-3) unstable; urgency=medium

  * Standards-Version to 4.1.3
  * Move to debhelper 11
  * Move VCS to salsa.debian.org
  * Rename OpenSSL 1.1 patch to give it a more useful name

 -- Eric Dorland <eric@debian.org>  Sat, 24 Feb 2018 19:18:16 -0500

pam-p11 (0.1.6-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Cherrypick upstream patch for OpenSSL 1.1 compat. (Closes: #871939)

 -- Dimitri John Ledkov <xnox@ubuntu.com>  Mon, 29 Jan 2018 11:13:37 +0000

pam-p11 (0.1.6-2) unstable; urgency=medium

  * Upload to unstable.
  * debian/control: Standards-Version to 4.0.0.

 -- Eric Dorland <eric@debian.org>  Thu, 06 Jul 2017 14:43:26 -0400

pam-p11 (0.1.6-1) experimental; urgency=medium

  * New upstream release.
  * debian/gbp.conf: Add upstream-vcs-tag.
  *
    debian/patches/0001-Use-INSTALL-instead-of-libLTLIBRARIES_INSTALL.patch:
    Drop now unnecessary patch.
  * debian/libpam-p11.docs: Drop now removed docs.
  * debian/rules: Remove ChangeLog install, it's gone.

 -- Eric Dorland <eric@debian.org>  Mon, 12 Jun 2017 23:40:10 -0400

pam-p11 (0.1.5-7) unstable; urgency=medium

  * debian/patches/0002-Read-certs-again-on-token-login.patch: Read certs
    again on token login. Thanks Sam Hartman. (Closes: #852039)

 -- Eric Dorland <eric@debian.org>  Wed, 25 Jan 2017 01:08:12 -0500

pam-p11 (0.1.5-6) unstable; urgency=medium

  * debian/control: Explicit build-deps on libssl1.0-dev.
  * debian/control: Standards-Version to 3.9.8.
  * debian/compat, debian/control, debian/rules: Switch to debhelper 10.
  *
    debian/patches/0001-Use-INSTALL-instead-of-libLTLIBRARIES_INSTALL.patch:
    Patch to us $(INSTALL) instead of $(libLTLIBRARIES_INSTALL).

 -- Eric Dorland <eric@debian.org>  Sat, 10 Dec 2016 20:32:54 -0500

pam-p11 (0.1.5-5) unstable; urgency=medium

  * debian/gbp.conf: Use pristine-tar.
  * debian/libpam-p11.docs: Run wrap-and-sort.
  * debian/control: Switch maintainer to pkg-opensc-maint@ and make myself
    an uploader.
  * debian/control, debian/rules: Use dh-autoreconf.

 -- Eric Dorland <eric@debian.org>  Sun, 10 May 2015 21:28:50 -0400

pam-p11 (0.1.5-4) unstable; urgency=medium

  * debian/control: Bump Standards-Version to 3.9.6 (no changes).
  * debian/control: Canonicalize Vcs-* fields.
  * debian/control: Update Homepage field to GitHub home.
  * debian/watch: Update for new GitHub home.
  * debian/control: Run wrap-and-sort.

 -- Eric Dorland <eric@debian.org>  Fri, 10 Oct 2014 23:24:29 -0400

pam-p11 (0.1.5-3) unstable; urgency=low

  * debian/copyright: Update to DEP5 copyright file.
  * debian/control, debian/rules: Use autotools-dev.
  * debian/control: Upgrade Standards-Version to 3.9.4.

 -- Eric Dorland <eric@debian.org>  Sat, 21 Sep 2013 17:39:01 -0400

pam-p11 (0.1.5-2) unstable; urgency=low

  * debian/compat, debian/control, debian/rules: Convert to debhelper 9
    and drop cdbs.
  * debian/control: Upgrade Standards-Version to 3.9.3.
  * debian/control, debian/rules: Multi-Arch enable package.
  * debian/source/format: Convert to 3.0 quilt source package format.

 -- Eric Dorland <eric@debian.org>  Sat, 10 Mar 2012 21:05:34 -0500

pam-p11 (0.1.5-1) unstable; urgency=low

  * New upstream release.
  * debian/rules:
    - Update Standards-Version to 3.8.1.
    - Change libltdl3-dev build-depends to just libltdl-dev.
    - Add ${misc:Depends}

 -- Eric Dorland <eric@debian.org>  Sun, 12 Apr 2009 03:13:02 -0400

pam-p11 (0.1.3-2) unstable; urgency=low

  * src/pam_p11.c: Patch from Nicolas François to remove incorrect usages
    of openlog/closelog/syslog with pam_syslog. (Closes: #472986)
  * debian/control:
    - Standards-Version to 3.7.3.
    - Add Homepage and Vcs-* headers.

 -- Eric Dorland <eric@debian.org>  Sat, 26 Apr 2008 01:16:39 -0400

pam-p11 (0.1.3-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Bump Standards-Version to 3.7.2.2.

 -- Eric Dorland <eric@debian.org>  Sat, 21 Jul 2007 15:40:30 -0400

pam-p11 (0.1.2-3) unstable; urgency=low

  * debian/watch: Update watchfile with new location.
  * debian/control: Standards-Version to 3.7.2.1.

 -- Eric Dorland <eric@debian.org>  Fri, 21 Jul 2006 23:22:44 -0400

pam-p11 (0.1.2-2) unstable; urgency=low

  * debian/copyright: Fix copyright so it correctly says this is licensed
    under the LGPL.

 -- Eric Dorland <eric@debian.org>  Sat, 31 Dec 2005 03:42:19 -0500

pam-p11 (0.1.2-1) unstable; urgency=low

  * Initial release.

 -- Eric Dorland <eric@debian.org>  Mon, 26 Dec 2005 02:31:31 -0500
